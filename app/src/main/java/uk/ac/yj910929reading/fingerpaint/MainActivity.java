package uk.ac.yj910929reading.fingerpaint;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    DrawingView dv ;
    private Paint mPaint;

    public enum COLOR {RED, BLUE, GREEN};
    public enum SIZE {SMALL, MEDIUM, LARGE};

    public COLOR brushColor;
    public SIZE brushSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        dv = (DrawingView)findViewById(R.id.DV_mainView);

        // the way these buttons work, they will cycle through the 3 options
        // small, medium, large && red, blue, yellow
        Button btnSize = (Button)findViewById(R.id.BTN_nextSize);
        Button btnColor = (Button)findViewById(R.id.BTN_nextColour);

        // set text on buttons
        btnSize.setText(R.string.btn_size);
        btnColor.setText(R.string.btn_color);

        // initialise brush variables
        brushColor = COLOR.RED;
        brushSize = SIZE.SMALL;

        updateColor();
        updateSize();

        // handles button clicks for colour
        btnColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateColor();
            }
        });

        // handles button clicks for size
        btnSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateSize();
            }
        });

    }

    public void updateColor(){
        switch(brushColor){
            case RED:
                brushColor = COLOR.BLUE;
                break;
            case BLUE:
                brushColor = COLOR.GREEN;
                break;
            case GREEN:
                brushColor = COLOR.RED;
                break;

                default:
                    break;
        }

        dv.setmPaintColor(brushColor);
    }

    // when this method is called, update brushSize to next value
    public void updateSize(){

        switch(brushSize){
            case SMALL:
                brushSize = SIZE.MEDIUM;
                break;
            case MEDIUM:
                brushSize = SIZE.LARGE;
                break;
            case LARGE:
                brushSize = SIZE.SMALL;
                break;

                default:
                    break;
        }

        dv.setmPaintSize(brushSize);
    }
}