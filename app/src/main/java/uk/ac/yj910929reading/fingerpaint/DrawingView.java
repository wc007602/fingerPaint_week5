package uk.ac.yj910929reading.fingerpaint;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Drawing view implements our painting app,
 * it acts like the UI elements (button, text view etc)
 * which are also subclasses of the View class (or sometimes subclasses of subclasses)
 */
public class DrawingView extends View {

        //Member variables we need to make our variables
        public int width;
        public  int height;
        private Bitmap mBitmap;
        private Canvas mCanvas;
        private Path mPath;
        private Paint mPaint;
        private Paint   mBitmapPaint;
        Context context;
        private Paint circlePaint;
        private Path circlePath;


        //constructor of the extended view must take two arguments a Context and AttributeSet
        //these must be passed to the superclass constructor
        public DrawingView(Context c, AttributeSet attrs) {
            super(c,attrs);

            context=c;
            mPath = new Path();
            mBitmapPaint = new Paint(Paint.DITHER_FLAG);
            circlePaint = new Paint();
            circlePath = new Path();
            circlePaint.setAntiAlias(true);
            circlePaint.setColor(Color.BLUE);
            circlePaint.setStyle(Paint.Style.STROKE);
            circlePaint.setStrokeJoin(Paint.Join.MITER);
            circlePaint.setStrokeWidth(4f);

            //set up the 'paint' object for the line we are going to draw
            mPaint = new Paint();
            mPaint.setAntiAlias(true);
            mPaint.setDither(true);
            mPaint.setColor(Color.GREEN);
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setStrokeJoin(Paint.Join.ROUND);
            mPaint.setStrokeCap(Paint.Cap.ROUND);
            mPaint.setStrokeWidth(12);
        }


        //function that is called if the screen orientation changes (or
        //this particular view is resized for any other reason)
        @Override
        protected void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);

            mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            mCanvas = new Canvas(mBitmap);

            width=w;
            height=h;
        }

        //Function that is called when the canvas is drawn
        //we know this will be called if we call the invalidate()
        //function
        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            canvas.drawBitmap( mBitmap, 0, 0, mBitmapPaint);
            canvas.drawPath( mPath,  mPaint);
            canvas.drawPath( circlePath,  circlePaint);


        }

        //###################################################################################
        //############################## My new methods #####################################
        //###################################################################################

        public void setmPaintColor(MainActivity.COLOR c){
            switch(c){
                case RED:
                    mPaint.setColor(Color.RED);
                    break;
                case BLUE:
                    mPaint.setColor(Color.BLUE);
                    break;
                case GREEN:
                    mPaint.setColor(Color.GREEN);
                    break;
            }
        }

        public void setmPaintSize(MainActivity.SIZE s){
            switch(s){
                case SMALL:
                    mPaint.setStrokeWidth(4f);
                    break;
                case MEDIUM:
                    mPaint.setStrokeWidth(20f);
                    break;
                case LARGE:
                    mPaint.setStrokeWidth(40f);
                    break;

                    default:
                        break;
            }
        }


        ////////////////////////////////////////////////////////////////////////////////////////
        /////////// Code Beneath here is related to handling the touch events //////////////////
        ////////////////////////////////////////////////////////////////////////////////////////


        //Touch Variables
        private float mX, mY;
        private static final float TOUCH_TOLERANCE = 4;

        //when the user first touches the screen we will call this function
        private void touch_start(float x, float y) {
            mPath.reset();
            mPath.moveTo(x, y);
            mX = x;
            mY = y;
        }

        //when the user is dragging their finger accross the screen
        private void touch_move(float x, float y) {
            float dx = Math.abs(x - mX);
            float dy = Math.abs(y - mY);
            if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                mPath.quadTo(mX, mY, (x + mX)/2, (y + mY)/2);
                mX = x;
                mY = y;

                circlePath.reset();
                circlePath.addCircle(mX, mY, 30, Path.Direction.CW);
            }
        }

        //function to perform when the user has stopped interacting by touch
        private void touch_up() {
            mPath.lineTo(mX, mY);
            circlePath.reset();
            // commit the path to our offscreen
            mCanvas.drawPath(mPath,  mPaint);
            // kill this so we don't double draw
            mPath.reset();
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float x = event.getX();
            float y = event.getY();


            //handle the different touch events by directing them to our previously declared
            //functions
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    touch_start(x, y);
                    invalidate();
                    break;
                case MotionEvent.ACTION_MOVE:
                    touch_move(x, y);
                    invalidate();
                    break;
                case MotionEvent.ACTION_UP:
                    touch_up();
                    invalidate();
                    break;
            }
            return true;
        }
}


